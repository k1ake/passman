#ifndef MASTER_WINDOW
#define MASTER_WINDOW

#include <gtkmm.h>

class MasterWindow : public Gtk::Window
{
public:
	MasterWindow(std::string filename);
	~MasterWindow() override;

protected:
	// Управление сигналами
	void on_confirm_click();
	void on_cancel_click();
	bool on_window_key_pressed(guint keyval, guint keycode, Gdk::ModifierType state);

	// Упаковки
	Gtk::Box m_VBox;
	Gtk::Box m_Button_HBox;

	// Дети
	Gtk::Label m_Label_welcome, m_Label_wrong_password;
	Gtk::Button m_button_cancel, m_button_confirm;
	Gtk::Entry m_Entry_mpass;

	std::string FILENAME;

	Glib::RefPtr<Gtk::EventControllerKey> controller = Gtk::EventControllerKey::create();
};

#endif