# Разработка

## Важное

<strong>PassMan</strong> - минималистичный графический менеджер паролей

1) По возможности не подключать сторонние библиотеки сверх crypto++ и gtkmm
2) В первую очередь баги, во вторую рефакторинг, и только потом новые фичи

## Структура

### Краткое описание файлов

<u>GUI</u>

+ new_master_window.h - Окно создания новой связки
+ master_window.h - Окно получения доступа к существующей связке
+ main_window.h - Главное окно программы
+ new_pass.h - Окно добавления пароля
+ change_pass.h - Окно изменения существующего пароля
+ delete_pass.h - Окно удаления существующего пароля

<u> Библиотеки </u>

+ store_enc.h - всё что связано с шифрованием и хранением пароля
  + AES_cipher_size - Хранит размер структуры выше
  + AES_cipher - Структура для хранения зашифрованного пароля
  + Encrypt - Зашифровывает пароль
  + Decrypt -  Расшифровывает пароль
  + store_AES - Сохраняет пароль в файл
  + restore_AES - Восстанавливает пароль из файла по его id
  + list_of_passwords - Получает список сохранённых паролей
  + id_by_name - Получает id пароля из его имени
  + change_password - Функция замены пароля в файле
  + delete_password - Удаляет пароль из файла
  + print_passwords -  Функция для вывода сохранённых паролей

+ master.h - всё что связано с доступом к паролям
  + get_md5 - Получить md5 хэш из некоторой строки
  + get_filename - Получает уникальное имя для файла
  + create_master - Функция для создания мастер-пароля и файла для сохранения паролей
  + check_master_exists - Функция проверки наличия файла с паролями
  + check_master_password - Функция для получения доступа к связке паролей

+ passgen.h - всё что связано с генератором паролей
  + Letters - Структура с допустимыми символами
  + generate_sequence - Создаёт новую строку из случайных элементов введёной строки
  + shuffle - перемешивает входную строку
  + generate_password - Генерирует пароль в зависимости от кол-ва символов и сложности

## Куда смотреть?

+ [gtkmm](https://gtkmm.org/en/index.html)

+ [Простейшие примеры gtkmm](https://developer-old.gnome.org/gtkmm-tutorial/stable/index.html)

+ [gtkmm доки по классам](https://developer-old.gnome.org/gtkmm/unstable/annotated.html)

+ [Ещё доки gtkmm](https://lazka.github.io/pgi-docs/Gtk-4.0/index.html)

+ [Как пользоваться cmake](https://habr.com/ru/post/461817/)

+ Пример подключения CryptoPP в cmake
  + [Раз](https://gitlab.com/kovri-project/kovri/-/blob/master/cmake/FindCryptoPP.cmake)
  + [Два](https://gitlab.com/kovri-project/kovri/-/blob/master/CMakeLists.txt)
