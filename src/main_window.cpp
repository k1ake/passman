#include "main_window.h"
#include "new_pass.h"
#include "delete_pass.h"
#include "change_pass.h"

#include <iostream>
#include <gtkmm.h>

#include "store_enc.h"
#include "passgen.h"
#include <vector>

MainWindow::MainWindow(std::string filename)
	: m_button_delete("Delete"),
	  m_button_change("Change"),
	  m_button_new("New"),
	  m_button_list_copy("Copy"),
	  m_ListButtonsHBox(Gtk::Orientation::HORIZONTAL),

	  m_button_generate("Generate"),
	  m_button_gen_copy("Copy"),
	  m_GenButtonsHBox(Gtk::Orientation::HORIZONTAL),

	  m_ComplexityHBox(Gtk::Orientation::HORIZONTAL),
	  m_LengthHBox(Gtk::Orientation::HORIZONTAL),
	  m_ListVBox(Gtk::Orientation::VERTICAL),
	  m_GenVBox(Gtk::Orientation::VERTICAL),
	  m_MainVBox(Gtk::Orientation::VERTICAL)
{

	FILENAME = filename;

	set_title("Main Window");
	set_default_size(350, 450);
	m_MainVBox.set_margin(5);
	m_MainVBox.set_vexpand(true);
	m_MainVBox.set_hexpand(true);

	set_child(m_MainVBox);

	// Создаём стак и свитчер, доавляем в главный бокс
	m_Switcher.set_stack(m_Stack);
	m_Switcher.set_margin(5);
	m_Stack.add(m_ListVBox, "ListPass", "Stored Passwords");
	m_Stack.add(m_GenVBox, "GenPass", "Password Generator");
	m_Stack.set_margin(5);
	m_MainVBox.append(m_Switcher);
	m_MainVBox.append(m_Stack);

	// Создаём страничку для списка паролей
	m_button_delete.set_margin(5);
	m_button_change.set_margin(5);
	m_button_new.set_margin(5);
	m_button_list_copy.set_margin(5);
	m_ListButtonsHBox.append(m_button_delete);
	m_ListButtonsHBox.append(m_button_change);
	m_ListButtonsHBox.append(m_button_new);
	m_ListButtonsHBox.append(m_button_list_copy);
	m_ListButtonsHBox.set_margin(5);
	m_ListButtonsHBox.set_halign(Gtk::Align::CENTER);

	// Добавляем всё в страничку со списком паролей
	// Добавляем к скроллу дерево с паролями
	m_ScrolledWindow.set_child(m_TreeView);
	// Показываем скроллбар только когда он нужен
	m_ScrolledWindow.set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC);
	m_ScrolledWindow.set_expand();
	m_ScrolledWindow.set_margin(5);

	// Создаём модельку для списка паролей
	m_refTreeModel = Gtk::ListStore::create(m_Columns);
	m_TreeView.set_model(m_refTreeModel);

	// Заполняем модельку
	// std::vector<std::string> password_list = list_of_passwords(FILENAME);
	fill_password_list();

	m_ListVBox.append(m_ScrolledWindow);

	m_Label_error.set_markup("<span color='red'>Error!</span>");
	m_MainVBox.append(m_Label_error);
	m_Label_error.hide();
	m_ListVBox.append(m_ListButtonsHBox);

	// Создаём страничку для генератора паролей
	m_GenButtonsHBox.set_margin(5);
	m_button_generate.set_margin(5);
	m_button_gen_copy.set_margin(5);

	m_GenButtonsHBox.append(m_button_generate);
	m_GenButtonsHBox.append(m_button_gen_copy);

	// Бокс для сложности пароля
	m_Label_complexity.set_markup("Password complexity level\n<span size='x-small' style='italic'>Higher is better</span>");
	m_Label_complexity.set_margin(5);
	m_spin_complexity.set_digits(0);
	m_spin_complexity.set_increments(1, 1);
	m_spin_complexity.set_range(0, 3);
	m_spin_complexity.set_margin(5);
	m_ComplexityHBox.append(m_Label_complexity);
	m_ComplexityHBox.append(m_spin_complexity);
	m_ComplexityHBox.set_margin(5);
	m_ComplexityHBox.set_hexpand(true);

	// Бокс для длины пароля
	m_Label_length.set_markup("Password length");
	m_Label_length.set_margin(5);
	m_spin_length.set_digits(0);
	m_spin_length.set_increments(1, 5);
	m_spin_length.set_range(2, 99);
	m_spin_length.set_value(12);
	m_spin_length.set_margin(5);
	m_LengthHBox.append(m_Label_length);
	m_LengthHBox.append(m_spin_length);
	m_LengthHBox.set_margin(5);

	// Добавляем всё в страничку с генератором
	m_Label_generated_pass.set_markup("");
	m_GenVBox.append(m_ComplexityHBox);
	m_GenVBox.append(m_LengthHBox);
	m_GenVBox.append(m_Label_generated_pass);
	m_Label_generated_pass.set_vexpand(true);
	m_GenVBox.append(m_Label_generator_error);
	m_Label_generator_error.hide();
	m_GenVBox.append(m_GenButtonsHBox);
	m_GenButtonsHBox.set_halign(Gtk::Align::END);
	m_GenVBox.set_margin(5);

	// Кнопочки
	m_button_list_copy.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &MainWindow::on_copy_click)));

	m_button_new.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &MainWindow::on_new_click)));

	m_button_delete.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &MainWindow::on_delete_click)));

	m_button_change.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &MainWindow::on_change_click)));

	m_button_generate.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &MainWindow::on_generate_click)));

	m_button_gen_copy.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &MainWindow::on_generate_copy_click)));
}

MainWindow::~MainWindow() {}

void MainWindow::fill_password_list()
{
	m_TreeView.remove_all_columns();
	m_refTreeModel->clear();
	// Заполняем модельку
	std::vector<std::string> password_list = list_of_passwords(FILENAME);
	for (int i = 1; i < password_list.size(); i++)
	{
		auto row = *(m_refTreeModel->append());
		row[m_Columns.m_col_id] = i;
		row[m_Columns.m_col_name] = password_list[i];
	}

	// Закидываем столбцы в список
	m_TreeView.append_column("№", m_Columns.m_col_id);
	m_TreeView.append_column("Имя", m_Columns.m_col_name);
}

void MainWindow::on_copy_click()
{
	auto refTreeSelection = m_TreeView.get_selection();
	auto row = *(refTreeSelection->get_selected());

	Glib::ustring alias = row[m_Columns.m_col_name];

	get_clipboard()->set_text(password_by_name(FILENAME, alias));
}

void MainWindow::on_delete_click()
{
	auto app = Gtk::Application::create("org.k1ake.delete_password");
	auto refTreeSelection = m_TreeView.get_selection();
	auto row = *(refTreeSelection->get_selected());

	if (row)
	{
		m_Label_error.hide();
		app->make_window_and_run<DeletePassWindow>(0, nullptr, FILENAME, row[m_Columns.m_col_name]);
		fill_password_list();
	}
	else
	{
		m_Label_error.set_markup("<span color='red'>Please choose password to delete it!</span>");
		m_Label_error.show();
	}
}
void MainWindow::on_new_click()
{
	auto app = Gtk::Application::create("org.k1ake.new_password");
	app->make_window_and_run<NewPassWindow>(0, nullptr, FILENAME);
	fill_password_list();
}

void MainWindow::on_change_click()
{
	auto app = Gtk::Application::create("org.k1ake.change_password");
	auto refTreeSelection = m_TreeView.get_selection();
	auto row = *(refTreeSelection->get_selected());

	if (row)
	{
		m_Label_error.hide();
		app->make_window_and_run<ChangePassWindow>(0, nullptr, FILENAME, row[m_Columns.m_col_name]);
		fill_password_list();
	}
	else
	{
		m_Label_error.set_markup("<span color='red'>Please choose password to change it!</span>");
		m_Label_error.show();
	}
}

void MainWindow::on_generate_click()
{
	int complexity = m_spin_complexity.get_value();
	int length = m_spin_length.get_value();
	m_Label_generated_pass.set_markup("<span color='red' size='xx-large'>" + generate_password(length, complexity) + "</span>");
	m_Label_generated_pass.show();
}

void MainWindow::on_generate_copy_click()
{
	if (m_Label_generated_pass.get_text() != "")
	{
		m_Label_generator_error.hide();
		get_clipboard()->set_text(m_Label_generated_pass.get_text());
	}
	else
	{
		m_Label_generator_error.set_markup("<span color='red'>Nothing to copy</span>");
		m_Label_generator_error.show();
	}
}