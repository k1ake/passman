#include "master.h"

#include <gtkmm/application.h>
#include "master_window.h"
#include "new_master_window.h"

using namespace std;

int main(int argc, char *argv[])
{
	auto app = Gtk::Application::create("org.k1ake.master");
	std::string filename = get_filename();
	if (getenv("HOME"))
	{
		// std::cout << "linux" << std::endl;
		filename = getenv("HOME") + std::string("/.config/") + filename;
		// std::cout << filename << std::endl;
	}
	// На будущее, когда и если будет билд для винды
	else if (getenv("APPDATA"))
	{
		// std::cout << "windows" << std::endl;
		filename = getenv("APPDATA") + std::string("\\") + filename;
		// std::cout << filename << std::endl;
	}
	else
	{
		perror("unknown system");
		return 0;
	}
	if (check_master_exists(filename))
		return app->make_window_and_run<MasterWindow>(argc, argv, filename);
	else
		return app->make_window_and_run<NewMasterWindow>(argc, argv, filename);
}