#ifndef CHANGE_PASS_WINDOW
#define CHANGE_PASS_WINDOW

#include <gtkmm.h>

class ChangePassWindow : public Gtk::Window
{
public:
	ChangePassWindow(std::string filename, Glib::ustring alias1);
	~ChangePassWindow() override;

protected:
	// Управление сигналами
	void on_confirm_click(const Glib::ustring &data);
	void on_cancel_click();
	void on_check_toggle();

	// Упаковки
	Gtk::Box m_VBox; // Вертикальная коробка
	Gtk::Box m_HBox; // Горизонтальная коробка

	// Дети
	Gtk::Label m_Label_alias, m_Label_pass, m_Label_pass_confirm, m_Label_error, m_Label_info;
	Gtk::Entry m_Entry_alias, m_Entry_pass, m_Entry_pass_confirm;
	Gtk::Button m_button_confirm, m_button_cancel;
	Gtk::CheckButton m_check_button_alias_only;

	std::string FILENAME;
	std::string ALIAS;
};

#endif