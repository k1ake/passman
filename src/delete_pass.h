#ifndef DELETE_PASS_WINDOW
#define DELETE_PASS_WINDOW

#include <gtkmm.h>

class DeletePassWindow : public Gtk::Window
{
public:
	DeletePassWindow(std::string filename, Glib::ustring alias);
	~DeletePassWindow() override;

protected:
	// Управление сигналами
	void on_confirm_click(const Glib::ustring &data);
	void on_cancel_click();

	// Упаковки
	Gtk::Box m_VBox;
	Gtk::Box m_HBox;

	// Дети
	Gtk::Label m_Label_instruction, m_Label_error;
	Gtk::Entry m_Entry_alias;
	Gtk::Button m_button_confirm, m_button_cancel;

	std::string FILENAME;
};
#endif