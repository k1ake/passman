#include "master_window.h"
#include "main_window.h"

#include "master.h"

#include <gtkmm.h>

MasterWindow::MasterWindow(std::string filename)
	: m_button_cancel("Cancel"),
	  m_button_confirm("Confirm"),
	  m_VBox(Gtk::Orientation::VERTICAL),
	  m_Button_HBox(Gtk::Orientation::HORIZONTAL)
{
	FILENAME = filename;
	set_title("PassMan");
	set_default_size(350, 150);
	set_resizable(false);
	set_child(m_VBox);
	set_default_widget(m_button_confirm);

	m_VBox.set_margin(10);
	m_VBox.set_vexpand(true);

	m_Label_welcome.set_markup("Welcome!\nInput your master password");
	m_Label_welcome.set_justify(Gtk::Justification::CENTER);
	m_Label_welcome.set_margin(5);
	m_VBox.append(m_Label_welcome);

	m_Entry_mpass.set_max_length(99);
	m_Entry_mpass.set_placeholder_text("Your password here...");
	m_Entry_mpass.set_margin(5);
	m_Entry_mpass.set_visibility(false);
	m_Entry_mpass.set_invisible_char('*');
	m_Entry_mpass.set_activates_default(true);
	m_VBox.append(m_Entry_mpass);

	m_Label_wrong_password.set_markup("");
	m_Label_wrong_password.hide();
	m_Label_wrong_password.set_justify(Gtk::Justification::CENTER);
	m_VBox.append(m_Label_wrong_password);

	m_Button_HBox.set_halign(Gtk::Align::END);
	m_button_cancel.set_margin(5);
	m_Button_HBox.append(m_button_cancel);
	m_button_confirm.set_margin(5);
	m_Button_HBox.append(m_button_confirm);
	m_VBox.append(m_Button_HBox);

	// Кнопочки
	m_button_cancel.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &MasterWindow::on_cancel_click)));
	m_button_confirm.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &MasterWindow::on_confirm_click)));

	// Управление клавишами
	controller->signal_key_pressed().connect(
		sigc::mem_fun(*this, &MasterWindow::on_window_key_pressed), false);
	add_controller(controller);
};
MasterWindow::~MasterWindow() {}

void MasterWindow::on_confirm_click()
{
	auto app = Gtk::Application::create("org.k1ake.main");
	if (!check_master_password(FILENAME, m_Entry_mpass.get_text()))
	{
		m_Label_wrong_password.set_markup("<span color='red'>Wrong password!</span>");
		m_Label_wrong_password.show();
		return;
	}
	hide();
	std::cout << FILENAME << std::endl;
	app->make_window_and_run<MainWindow>(0, nullptr, FILENAME);
}
void MasterWindow::on_cancel_click()
{
	std::printf("Cancel\n");
	hide();
}

bool MasterWindow::on_window_key_pressed(guint keyval, guint, Gdk::ModifierType state)
{
	// select the first radio button, when we press alt + 1
	if (keyval == GDK_KEY_Escape)
	{
		// close the window, when the 'esc' key is pressed
		hide();
		return true;
	}

	return false;
}