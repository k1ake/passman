#include "passgen.h"

#include <random>

/// @brief Хранит все допустимые символы для пароля
const struct
{
	std::string lower = "abcdefghijklmnopqrstuvwxyz";
	std::string upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	std::string digits = "0123456789";
	std::string extras = ".,!#$%:-_";
} Letters;

/// @brief Выбирает из строки случайные элементы в некотором количестве
/// @param possibles Исходная строка
/// @param size Размер выходной строки
/// @return Строку из случайных элементов
std::string generate_sequence(std::string possibles, int size)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dist(0, possibles.size() - 1);

	std::string ans;

	while (ans.size() < size)
		ans.push_back(possibles[dist(gen)]);
	return ans;
}

/// @brief Перемешивает строку
/// @param input Строка для перемешивания
/// @return Перемешанная строка
std::string shuffle(std::string input)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::string answer;
	std::string::iterator it;

	while (input.size() > 0)
	{
		std::uniform_int_distribution<> dist(0, input.size() - 1);
		it = input.begin() + dist(gen);
		answer.push_back(input[distance(input.begin(), it)]);
		input.erase(it);
	}

	return answer;
}

/// @brief Генерирует пароль в соответствии с необходимой длиной и сложностью
/// @param size Длина пароля
/// @param level Сложность пароля:
/// 			0 - только маленькие буквы
/// 			1 - маленькие и большие буквы
/// 			2 - буквы + цифры
/// 			3 - буквы, цифры, спецц. символы
/// @return Сгенерированный пароль
std::string generate_password(int size, int level)
{
	std::string output;
	int part_size;
	if (level == 0)
	{
		output = generate_sequence(Letters.lower, size);
	}
	else if (level == 1)
	{
		part_size = size / 2;
		output = generate_sequence(Letters.lower, part_size);
		output += generate_sequence(Letters.upper, size - part_size);
	}
	else if (level == 2)
	{
		part_size = size / 3;
		output = generate_sequence(Letters.lower, part_size);
		output += generate_sequence(Letters.upper, part_size);
		output += generate_sequence(Letters.digits, size - 2 * part_size);
	}
	else
	{
		part_size = size / 4;
		output = generate_sequence(Letters.lower, part_size);
		output += generate_sequence(Letters.upper, part_size);
		output += generate_sequence(Letters.digits, part_size);
		output += generate_sequence(Letters.extras, size - 3 * part_size);
	}

	std::string password = shuffle(output);

	return password;
}
