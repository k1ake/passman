#include "master.h"
#include "store_enc.h"

#include <experimental/filesystem>
#include <cryptopp/hex.h>
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>

#include <cryptopp/modes.h>
#include <cryptopp/aes.h>
#include <cryptopp/filters.h>

/// @brief Получаем имя пользователя
std::string MASTER_NAME = getpwuid(geteuid())->pw_name;

/// @brief Получить MD5 хэш
/// @param msg Что мы преобразуем в хэш
/// @return Хэш
std::string get_md5(std::string msg)
{
	using namespace CryptoPP;

	std::string digest, undigest;
	Weak::MD5 hash;

	StringSource ss(msg, true /*pumpAll*/, new HashFilter(hash, new HexEncoder(new StringSink(digest))));

	return digest;
}

/// @brief Функция для создания и сохранения мастер-пароля
/// @param filename Имя файла с паролями
void create_master(std::string filename, std::string password)
{
	std::string master_tmp;
	int l = password.size() / 2 + 2;
	master_tmp = password.substr(0, l);
	reverse(master_tmp.begin(), master_tmp.end());
	master_tmp = password + master_tmp;
	password = get_md5(master_tmp);

	master_tmp = "definetly_not_real_masterpass";
	store_AES(filename, Encrypt(password, master_tmp), 0);
}

/// @brief Функция проверки доступа к паролям
/// @param filename Имя файла с паролями
int check_master_exists(std::string filename)
{
	std::filesystem::path f{filename};
	if (std::filesystem::exists(f))
		return 1;
	else
		return 0;
}

int check_master_password(std::string filename, std::string password)
{

	std::string tmp;
	int l = password.size() / 2 + 2;
	tmp = password.substr(0, l);
	reverse(tmp.begin(), tmp.end());
	tmp = password + tmp;
	password = get_md5(tmp);
	if (Decrypt(restore_AES(filename, 0)) == password)
	{
		puts("Доступ получен");
		return 1;
	}
	perror("Неверный мастер-пароль");
	return 0;
}
/// @brief Получаем имя для файла с паролем нехитрыми преобразованиями над именем пользователя
/// @return Имя хранилища
std::string get_filename()
{
	int l = MASTER_NAME.size() / 2;
	std::string tmp = MASTER_NAME.substr(MASTER_NAME.size() - l, l);
	std::reverse(tmp.begin(), tmp.end());
	tmp += MASTER_NAME;
	return "." + get_md5(tmp);
}
