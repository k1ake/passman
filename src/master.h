#ifndef MASTER_HPP
#define MASTER_HPP

#include <pwd.h>
#include <unistd.h>
#include <iostream>
#include <filesystem>

std::string get_md5(std::string);
int check_master_exists(std::string filename);
int check_master_password(std::string filename, std::string password);
void create_master(std::string filename, std::string password);
std::string get_filename();
#endif