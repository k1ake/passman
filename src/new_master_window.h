#ifndef NEW_MASTER_WINDOW
#define NEW_MASTER_WINDOW

#include <gtkmm.h>

class NewMasterWindow : public Gtk::Window
{
public:
	NewMasterWindow(std::string filename);
	~NewMasterWindow() override;

protected:
	// Управление сигналами
	void on_create_click();
	void on_cancel_click();
	bool on_window_key_pressed(guint keyval, guint keycode, Gdk::ModifierType state);

	// Упаковки
	Gtk::Box m_VBox;
	Gtk::Box m_Button_HBox;

	// Дети
	Gtk::Label m_Label_welcome, m_Label_mpass, m_Label_mpass_confirm, m_Label_error;
	Gtk::Entry m_Entry_mpass, m_Entry_mpass_confirm;
	Gtk::Button m_button_cancel, m_button_create;

	std::string FILENAME;
	Glib::RefPtr<Gtk::EventControllerKey> controller = Gtk::EventControllerKey::create();
};

#endif