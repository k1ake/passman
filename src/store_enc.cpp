#include "store_enc.h"

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <vector>
// Криптографические библиотеки для AES и MD5
#include <cryptopp/hex.h>
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/md5.h>
#include "cryptopp/modes.h"
#include "cryptopp/aes.h"
#include "cryptopp/filters.h"

// Размер структуры
int AES_cipher_size = 2 * sizeof(char[128]) + CryptoPP::AES::DEFAULT_KEYLENGTH + CryptoPP::AES::BLOCKSIZE;

/// @brief Шифрует текст используя тип шифрования AES и режим CBC
/// @param plaintext Текст для шифрования (в случае этой программы пароль)
/// @param name Название для пароля, нужно для упрощения доступа к нему со стороны пользователя
/// @return Возвращает структуру хранящую всё необходимое для работы с паролем
AES_cipher Encrypt(std::string plaintext, std::string name)
{
	// Создаём структуру для шифровки
	AES_cipher enc;
	enc.name = name;
	memset(enc.key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH);
	memset(enc.iv, 0x00, CryptoPP::AES::BLOCKSIZE);

	//
	// Шифруемся
	//
	// Создаём класс/структуру объявляющую что будем шифровать с помощью AES
	CryptoPP::AES::Encryption aesEncryption(enc.key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	// Определяем режим CBC для нашего шифрования
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, enc.iv);

	// Открываем поток который запишет в ciphertext то что мы засунем
	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(enc.ciphertext));
	// Пихаем в поток наш plaintext и его длину
	stfEncryptor.Put(reinterpret_cast<const unsigned char *>(plaintext.c_str()), plaintext.length());
	// Закрываем поток
	stfEncryptor.MessageEnd();

	// Мешаем зашифрованный текст
	std::string p1, p2, p3, text = enc.ciphertext;
	p1 = text.substr(text.size() - 2, text.size() - 1);
	reverse(p1.begin(), p1.end());
	p2 = text.substr(2, text.size() - 4);
	p3 = text.substr(0, 2);
	enc.ciphertext = p1 + p2 + p3;
	return enc;
};

/// @brief Расшифровывает пароль и возвращает его же
/// @param enc Структура хранящая пароль ключ и IV
/// @return Строка содержащая пароль
std::string Decrypt(AES_cipher enc)
{
	// Заготовка под расшифрованный текст
	std::string decryptedtext;

	//  Отмешиваем зашифрованный текст
	std::string p1, p2, p3, text = std::string(enc.ciphertext);
	p1 = text.substr(text.size() - 2, text.size() - 1);
	p2 = text.substr(2, text.size() - 4);
	p3 = text.substr(0, 2);
	reverse(p3.begin(), p3.end());
	enc.ciphertext = p1 + p2 + p3;

	//
	// Дешифруемся
	//
	// Создаём класс/структуру под дешифровку
	CryptoPP::AES::Decryption aesDecryption(enc.key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	// Определяем метод расшифровки CBC
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, enc.iv);

	// Открываем поток который дешифрует в decryptedtext то что мы в него засунем
	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedtext));

	// Пихаем зашифрованный текст в поток и его длину
	stfDecryptor.Put(reinterpret_cast<const unsigned char *>(enc.ciphertext.c_str()), enc.ciphertext.size());
	// Закрываем поток
	stfDecryptor.MessageEnd();
	return decryptedtext;
};

/// @brief Сохраняет структуру с паролем в файл
/// @param filename имя файла, для использования
/// @param aes Структура с паролем
/// @param mode Режим записи если 0, то перезапись всего файла (по умолчанию задан 1)
void store_AES(std::string filename, AES_cipher aes, int mode)
{
	// Переменная под шифрованный пароль
	char cct[128];
	// Переменная под название пароля
	char cname[128];
	strcpy(cct, aes.ciphertext.c_str());
	strcpy(cname, aes.name.c_str());
	FILE *fout = fopen(filename.c_str(), (mode == 0 ? "w" : "a"));
	if (fout)
	{
		fwrite(&cct, sizeof(cct), 1, fout);
		fwrite(&cname, sizeof(cname), 1, fout);
		fclose(fout);
	}
	else
	{
		perror("Не удалось открыть файл для записи зашифрованного пароля или его имени\n");
		exit(1);
	}

	fout = fopen(filename.c_str(), "ab");
	if (fout)
	{
		fwrite(&aes.key, sizeof(aes.key), 1, fout);
		fwrite(&aes.iv, sizeof(aes.iv), 1, fout);
		fclose(fout);
	}
	else
	{
		perror("Не удалось открыть файл для записи ключа или iv\n");
		exit(1);
	}
};

/// @brief Восстановление структууры с паролем из файла
/// @param filename Название файла хранящего пароль
/// @param id Номер пароля для получения >0 !!
/// @return Возвращает структуру с паролем
AES_cipher restore_AES(std::string filename, int id)
{
	AES_cipher aes;
	char cct[128];
	int start_point = id * AES_cipher_size;

	FILE *fin = fopen(filename.c_str(), "r");
	if (fin)
	{
		if (id)
			fseek(fin, start_point, SEEK_SET);
		fread(&cct, sizeof(cct), 1, fin);
		aes.ciphertext = cct;
		fread(&cct, sizeof(cct), 1, fin);
		aes.name = cct;
		fclose(fin);
	}
	else
	{
		perror("Не прочитан шифр или псевдоним");
		exit(1);
	}

	fin = fopen(filename.c_str(), "rb");
	if (fin)
	{
		fseek(fin, 2 * sizeof(cct) + start_point, SEEK_SET);
		fread(&aes.key, sizeof(aes.key), 1, fin);
		fread(&aes.iv, sizeof(aes.iv), 1, fin);
		fclose(fin);
	}
	else
	{
		perror("Не прочитан ключ или iv");
		exit(1);
	}

	return aes;
};

/// @brief Получение списка паролей с названиями
/// @param filename  Название файла, в котором искать пароли
/// @return Возвращает указатель на вектор паролей
std::vector<std::string> list_of_passwords(std::string filename)
{
	int check, n;
	std::vector<std::string> ar;
	char cname[128];
	FILE *fin = fopen(filename.c_str(), "r");
	if (fin)
	{
		fseek(fin, 0, SEEK_END);
		n = ftell(fin) / AES_cipher_size;

		// Проверка на целостность файла
		check = ftell(fin) % AES_cipher_size;
		if (check)
			perror("Длина файла не правильная, целостность паролей может быть нарушена");
		fseek(fin, 0, SEEK_SET);
		for (int i = 0; i < n; i++)
		{
			fseek(fin, i * AES_cipher_size + sizeof(cname), SEEK_SET);
			fread(&cname, sizeof(cname), 1, fin);
			ar.push_back(cname);
		}
		fclose(fin);
	}
	else
	{
		perror(filename.c_str());
	}
	return ar;
}
/// @brief Получение номера пароля в связке по его имени
/// @param filename Название файла
/// @param name Имя пароля
/// @return Номер
int id_by_name(std::string filename, std::string name)
{
	std::vector<std::string> ar = list_of_passwords(filename);
	std::vector<std::string>::iterator it = find(ar.begin(), ar.end(), name);
	if (it == ar.end())
	{
		return -1;
	};
	int index = distance(ar.begin(), it);
	return index;
}
/// @brief Получение пароля по его названию
/// @param filename Имя файла с паролями
/// @param name Название пароля
/// @return Сам пароль
std::string password_by_name(std::string filename, std::string name)
{
	int index = id_by_name(filename, name);
	std::string ans = Decrypt(restore_AES(filename, index));
	return ans;
}

/// @brief Изменяет пароль уже хранящийся в базе
/// @param filename Название файла
/// @param name Название пароля, который нужно изменить
/// @param new_pass Новый пароль
/// @param new_name Новое название
void change_password(std::string filename, std::string name, std::string new_name, std::string new_pass)
{
	if (id_by_name(filename, name) < 0)
	{
		perror("Нет такого пароля");
		return;
	}

	if (new_name == "" && new_pass == "")
	{
		perror("Нечего менять");
		return;
	}

	int id = id_by_name(filename, name);
	int total_passwords = list_of_passwords(filename).size();
	std::string tmp_filename = filename + ".tmp_";
	CryptoPP::byte tmp[AES_cipher_size * total_passwords];

	AES_cipher aes;
	if (new_pass != "")
		aes = Encrypt(new_pass, (new_name != "" ? new_name : name));
	else
	{
		aes = restore_AES(filename, id);
		aes.name = new_name;
	}

	FILE *fin = fopen(filename.c_str(), "rb");
	FILE *fout = fopen(tmp_filename.c_str(), "wb");

	if (fin && fout)
	{
		fread(&tmp, AES_cipher_size, id, fin);
		fwrite(&tmp, AES_cipher_size, id, fout);
		fclose(fin);
		fclose(fout);
	}
	else
	{
		perror("Не удалось открыть файл для замены");
	}
	store_AES(tmp_filename, aes);
	fin = fopen(filename.c_str(), "rb");
	fout = fopen(tmp_filename.c_str(), "ab");
	if (fin && fout)
	{
		fseek(fin, AES_cipher_size * (id + 1), SEEK_SET);
		fread(&tmp, AES_cipher_size, total_passwords - id - 1, fin);
		fwrite(&tmp, AES_cipher_size, total_passwords - id - 1, fout);
		fclose(fin);
		fclose(fout);
	}
	else
	{
		perror("Не удалось открыть файл для замены");
	}
	if (remove(filename.c_str()) != 0)
		perror("Не удалось удалить временный файл");
	if (rename(tmp_filename.c_str(), filename.c_str()) != 0)
		perror("Не удалось переименовать базу");
}

/// @brief Удаляет пароль путём вырезки из файла куска байтов соответсвующего ему
/// @param filename Имя файла
/// @param name Название пароля
void delete_password(std::string filename, std::string name)
{
	AES_cipher aes;
	int id = id_by_name(filename, name);
	int total_passwords = list_of_passwords(filename).size();

	std::string tmp_filename = filename + ".tmp_";
	CryptoPP::byte tmp[AES_cipher_size * total_passwords];

	FILE *fin = fopen(filename.c_str(), "rb");
	FILE *fout = fopen(tmp_filename.c_str(), "wb");

	if (fin && fout)
	{
		fread(&tmp, AES_cipher_size, id, fin);
		fwrite(&tmp, AES_cipher_size, id, fout);
		fseek(fin, AES_cipher_size, SEEK_CUR);
		fread(&tmp, AES_cipher_size, total_passwords - id, fin);
		fwrite(&tmp, AES_cipher_size, total_passwords - id, fout);
		fclose(fin);
		fclose(fout);
	}
	else
	{
		perror("Не удалось открыть файл для удаления");
	}

	fin = fopen(tmp_filename.c_str(), "rb");
	fout = fopen(filename.c_str(), "wb");

	if (fin && fout)
	{
		fread(&tmp, AES_cipher_size, total_passwords - 1, fin);
		fwrite(&tmp, AES_cipher_size, total_passwords - 1, fout);
		fclose(fin);
		fclose(fout);
	}
	else
	{
		perror("Не удалось открыть файл для удаления");
	}

	if (remove(tmp_filename.c_str()) != 0)
		perror("Не удалось удалить временный файл");
}

/// @brief Функция для вывода паролей в терминал
/// @param filename название файла для поиска паролей
/// @param mode режим вывода пароля, при 0 выведет так же и хэш мастер-пароля
void print_passwords(std::string filename, int mode)
{
	std::vector<std::string> passwords = list_of_passwords(filename);
	if (passwords.size() <= 1 && mode != 0)
	{
		printf("Нет сохранённых паролей\n");
		return;
	}
	for (int i = (mode != 0 ? 1 : 0); i < passwords.size(); i++)
	{
		printf("%d) %s -> %s\n", i, passwords[i].c_str(), password_by_name(filename, passwords[i]).c_str());
	}
}
