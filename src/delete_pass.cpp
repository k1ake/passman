#include "delete_pass.h"
#include "store_enc.h"

#include <iostream>
#include <gtkmm.h>

DeletePassWindow::DeletePassWindow(std::string filename, Glib::ustring alias)
	: m_button_confirm("Confirm"),
	  m_button_cancel("Cancel"),
	  m_VBox(Gtk::Orientation::VERTICAL),
	  m_HBox(Gtk::Orientation::HORIZONTAL)
{
	FILENAME = filename;
	set_title("Password deletion");
	set_default_size(350, 160);
	set_resizable(false);
	set_modal();

	m_VBox.set_margin(10);
	m_VBox.set_vexpand(true);

	set_child(m_VBox);

	m_Label_instruction.set_markup("To confirm password deletion\nInput <span color='red' size='x-large'>" + alias + "</span> in the field below");
	m_Label_instruction.set_margin(5);
	m_VBox.append(m_Label_instruction);

	m_Entry_alias.set_max_length(99);
	m_Entry_alias.set_placeholder_text("...");
	m_Entry_alias.set_margin(5);
	m_VBox.append(m_Entry_alias);

	m_Label_error.set_markup("error");
	m_Label_error.hide();
	m_VBox.append(m_Label_error);

	m_button_confirm.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &DeletePassWindow::on_confirm_click), alias));
	m_button_cancel.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &DeletePassWindow::on_cancel_click)));

	m_HBox.append(m_button_cancel);
	m_HBox.append(m_button_confirm);
	m_button_cancel.set_margin(5);
	m_button_confirm.set_margin(5);
	m_HBox.set_halign(Gtk::Align::END);
	m_VBox.append(m_HBox);
};

void DeletePassWindow::on_confirm_click(const Glib::ustring &alias)
{
	if (alias == m_Entry_alias.get_text())
	{
		delete_password(FILENAME, alias);
		hide();
	}
	else
	{
		m_Label_error.set_markup("<span color='red'>Wrong</span>");
		m_Label_error.show();
	}
}
void DeletePassWindow::on_cancel_click() { hide(); }

DeletePassWindow::~DeletePassWindow()
{
}