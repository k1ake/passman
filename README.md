# passman

## О проекте

Минималистичный менеджер паролей

## Что уже есть

+ AES шифрование туда обратно, зашифрованный текст видоизменяется, что не позволит получить пароль не используя эту программу
+ Сохранение паролей в файл в любом количестве
+ Изменение сохранённых паролей
+ Удаление сохранённых паролей
+ Вывод в терминал списка ```Название -> Пароль```
+ Файл с паролями генерируется для каждого пользователя на системе, имеет крайне непримечательное название
+ Возможно получить md5 хэш для чего-нибудь
+ Защита связки мастер-паролем
+ Генерация пароля разной сложности
+ Рабочий GUI

## Сборка

### Зависимости

| Название  |         Arch        |       ~~Fedora~~            |
|-----------|---------------------|-------------------------|
| CryptoPP  | pacman -S crypto++  | ~~dnf install cryptopp~~    |
| gtkmm4    | pacman -S gtkmm-4.0 | ~~dnf install gtkmm4.0~~    |
| cmake     | pacman -S cmake     | ~~dnf install cmake~~       |
| ninja     | pacman -S ninja     | ~~dnf install ninja-build~~ |
| gcc       | pacman -S gcc       | ~~dnf install gcc~~         |
| g++       | appears with gcc    | ~~dnf install gcc-c++~~     |

```
git clone https://gitlab.com/k1ake/passman && cd passman
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_C_COMPILER:STRING=gcc -DCMAKE_CXX_COMPILER:STRING=g++ -S. -Bbuild -G Ninja

cmake --build ./build --config Release --target all
```

Ограничение на длину пароля и его имени 99 символов
