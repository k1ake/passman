#ifndef STOREENC_HPP
#define STOREENC_HPP

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <vector>

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cryptopp/hex.h>
#include <cryptopp/md5.h>
#include "cryptopp/modes.h"
#include "cryptopp/aes.h"
#include "cryptopp/filters.h"

/// @brief Структура хранящая в себе зашифрованный пароль, его название и ключ с IV для расшифровки
struct AES_cipher
{
	CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];
	std::string ciphertext;
	std::string name;
};

AES_cipher Encrypt(std::string plaintext, std::string name);
std::string Decrypt(AES_cipher enc);

void store_AES(std::string filename, AES_cipher aes, int mode = 1);

AES_cipher restore_AES(std::string filename, int id);
std::vector<std::string> list_of_passwords(std::string filename);

int id_by_name(std::string filename, std::string name);
std::string password_by_name(std::string filename, std::string name);

void change_password(std::string filename, std::string name, std::string new_name = "", std::string new_pass = "");

void delete_password(std::string filename, std::string name);

void print_passwords(std::string filename, int mode = 1);

#endif