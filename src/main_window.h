#ifndef MAIN_WINDOW
#define MAIN_WINDOW

#include <gtkmm.h>

class MainWindow : public Gtk::Window
{
public:
	MainWindow(std::string filename);
	~MainWindow();

protected:
	// Управление сигналами
	void on_delete_click();
	void on_change_click();
	void on_new_click();
	void on_copy_click();
	void on_switch_click();
	void on_generate_click();
	void on_generate_copy_click();
	void fill_password_list();

	// Упаковки
	Gtk::Box m_MainVBox, m_ListVBox, m_GenVBox;
	Gtk::Box m_ListButtonsHBox, m_GenButtonsHBox;
	Gtk::Box m_ComplexityHBox, m_LengthHBox;
	Gtk::Stack m_Stack;
	Gtk::StackSwitcher m_Switcher;

	// Дети
	Gtk::Button m_button_delete, m_button_change, m_button_new, m_button_list_copy;
	Gtk::Button m_button_generate, m_button_gen_copy;

	Gtk::SpinButton m_spin_complexity, m_spin_length;

	Gtk::Label m_Label_complexity, m_Label_length, m_Label_generated_pass, m_Label_error, m_Label_generator_error;

	// Создаём модель под список паролей
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns()
		{
			add(m_col_id);
			add(m_col_name);
		}

		Gtk::TreeModelColumn<unsigned int> m_col_id;
		Gtk::TreeModelColumn<Glib::ustring> m_col_name;
	};

	// Берём всё что нужно под список паролей
	ModelColumns m_Columns;
	Gtk::ScrolledWindow m_ScrolledWindow;
	Gtk::TreeView m_TreeView;
	Glib::RefPtr<Gtk::ListStore> m_refTreeModel;

	std::string FILENAME;
};

#endif