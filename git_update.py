# https://azzamsa.com/n/gitpython-intro/

import shutil, os, git

REPO_NAME = "passman"
REPO_PATH = os.environ["HOME"] + "/mygit/" + REPO_NAME + "/"


def git_update(repo_name: str):
    commit_msg = input("Commit message: ")

    repo = git.Repo(REPO_PATH)
    repo.git.add(REPO_PATH + "*")
    repo.git.commit(m=commit_msg)

    repo.git.push(f"git@gitlab.com:k1ake/{REPO_NAME}.git")
    print("Done")


def move_to_git(trees: str, files: str):
    for i in trees:
        shutil.copytree(i, REPO_PATH + i,
                        dirs_exist_ok=True, ignore=shutil.ignore_patterns(".vscode", "build"))
    for i in files:
        shutil.copyfile(i, REPO_PATH + i)


def main():
    trees_to_move = [
        "cmake",
        "docs",
        "src"
    ]
    files_to_move = [
        "CMakeLists.txt",
        "git_update.py",
        "README.md"
    ]
    move_to_git(trees_to_move, files_to_move)
    git_update(REPO_NAME)


if __name__ == "__main__":
    main()
