#include "new_pass.h"
#include "store_enc.h"
#include <iostream>
#include <gtkmm.h>

NewPassWindow::NewPassWindow(std::string filename)
	: m_button_confirm("Confirm"),
	  m_button_cancel("Cancel"),
	  m_VBox(Gtk::Orientation::VERTICAL),
	  m_HBox(Gtk::Orientation::HORIZONTAL)
{
	FILENAME = filename;
	set_title("Create new password!");
	set_default_size(350, 250);
	set_resizable(false);
	m_VBox.set_margin(10);
	m_VBox.set_vexpand(true);

	// Главный ребёнок окна - вертикальная коробка
	set_child(m_VBox);

	// Имя пароля, лейбл и поле ввода
	m_Label_alias.set_text("Input password alias:");
	m_Label_alias.set_margin(5);
	m_VBox.append(m_Label_alias);

	m_Entry_alias.set_max_length(99);
	m_Entry_alias.set_placeholder_text("Your alias here...");
	m_Entry_alias.set_margin(5);
	m_VBox.append(m_Entry_alias);

	// Пароль, лейбл и поле ввода
	m_Label_pass.set_text("Input password here:");
	m_Label_pass.set_margin(5);
	m_VBox.append(m_Label_pass);

	m_Entry_pass.set_max_length(99);
	m_Entry_pass.set_placeholder_text("Your password here...");
	m_Entry_pass.set_margin(5);
	m_Entry_pass.set_visibility(false);
	m_Entry_pass.set_invisible_char('*');
	m_VBox.append(m_Entry_pass);

	// Подтверждение пароля, лейбл и поле ввода
	m_Label_pass_confirm.set_text("Confirm your password here:");
	m_Label_pass_confirm.set_margin(5);
	m_VBox.append(m_Label_pass_confirm);

	m_Entry_pass_confirm.set_max_length(99);
	m_Entry_pass_confirm.set_placeholder_text("Your password again...");
	m_Entry_pass_confirm.set_margin(5);
	m_Entry_pass_confirm.set_visibility(false);
	m_Entry_pass_confirm.set_invisible_char('*');
	m_VBox.append(m_Entry_pass_confirm);

	// Кнопочки
	m_button_confirm.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &NewPassWindow::on_confirm_click), "confirm"));
	m_button_cancel.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &NewPassWindow::on_cancel_click)));

	m_HBox.append(m_button_cancel);
	m_button_cancel.set_margin(5);

	m_HBox.append(m_button_confirm);
	m_button_confirm.set_margin(5);

	m_HBox.set_halign(Gtk::Align::END);
	m_VBox.append(m_HBox);
	m_VBox.append(m_Label_error);
	m_Label_error.hide();
}

NewPassWindow::~NewPassWindow() {}

void NewPassWindow::on_confirm_click(const Glib::ustring &data)
{
	Glib::ustring alias = m_Entry_alias.get_text();
	Glib::ustring password = m_Entry_pass.get_text();

	// Проверка на использованность имени
	if (id_by_name(FILENAME, alias) != -1 || alias.size() == 0)
	{
		m_Label_error.set_markup("<span color='red'>That alias is already in use or field is empty!</span>");
		m_Label_error.show();
		return;
	}
	if (password == m_Entry_pass_confirm.get_text() && password.size() > 0)
	{
		store_AES(FILENAME, Encrypt(password, alias));
		hide();
	}
	else
	{
		m_Label_error.set_markup("<span color='red'>Passwords are different or fields are empty!</span>");
		m_Label_error.show();
	}

	// Воткнуть сохранение пароля
}
void NewPassWindow::on_cancel_click()
{
	hide();
	std::cout << "NewPass - Cancel was pressed" << std::endl;
}