#ifndef PASSGEN_H
#define PASSGEN_H

#include <random>
#include <iostream>

struct Letters;

std::string generate_sequence(std::string, int);

std::string shuffle(std::string);

std::string generate_password(int, int);

#endif
