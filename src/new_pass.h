#ifndef NEW_PASS_WINDOW
#define NEW_PASS_WINDOW

#include <gtkmm.h>

class NewPassWindow : public Gtk::Window
{
public:
	NewPassWindow(std::string filename);
	~NewPassWindow() override;

protected:
	// Управление сигналами
	void on_confirm_click(const Glib::ustring &data);
	void on_cancel_click();

	// Упаковки
	Gtk::Box m_VBox; // Вертикальная коробка
	Gtk::Box m_HBox; // Горизонтальная коробка

	// Дети
	Gtk::Label m_Label_alias, m_Label_pass, m_Label_pass_confirm, m_Label_error;
	Gtk::Entry m_Entry_alias, m_Entry_pass, m_Entry_pass_confirm;
	Gtk::Button m_button_confirm, m_button_cancel;

	std::string FILENAME;
};

#endif