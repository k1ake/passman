#include "change_pass.h"
#include "store_enc.h"

#include <iostream>
#include <gtkmm.h>

ChangePassWindow::ChangePassWindow(std::string filename, Glib::ustring alias)
	: m_button_confirm("Confirm"),
	  m_button_cancel("Cancel"),
	  m_check_button_alias_only("Change only alias"),
	  m_VBox(Gtk::Orientation::VERTICAL),
	  m_HBox(Gtk::Orientation::HORIZONTAL)
{
	FILENAME = filename;
	ALIAS = alias;
	set_title("Changing: " + ALIAS);
	set_default_size(350, 300);
	set_resizable(false);
	set_modal();
	m_VBox.set_margin(10);
	m_VBox.set_vexpand(true);

	// Главный ребёнок окна - вертикальная коробка
	set_child(m_VBox);

	// Имя пароля, лейбл и поле ввода
	m_Label_alias.set_text("Password alias:");
	m_Label_alias.set_margin(5);
	m_VBox.append(m_Label_alias);

	m_Entry_alias.set_max_length(99);
	m_Entry_alias.set_placeholder_text("New alias here...");
	m_Entry_alias.set_text(ALIAS);
	m_Entry_alias.set_margin(5);
	m_VBox.append(m_Entry_alias);

	m_check_button_alias_only.set_margin(5);
	m_check_button_alias_only.set_halign(Gtk::Align::END);
	m_VBox.append(m_check_button_alias_only);

	// Пароль, лейбл и поле ввода
	m_Label_pass.set_text("Input new password here:");
	m_Label_pass.set_margin(5);
	m_VBox.append(m_Label_pass);

	m_Entry_pass.set_max_length(99);
	m_Entry_pass.set_placeholder_text("Your new password here...");
	m_Entry_pass.set_margin(5);
	m_Entry_pass.set_visibility(false);
	m_Entry_pass.set_invisible_char('*');
	m_VBox.append(m_Entry_pass);
	m_Entry_pass.grab_focus();

	// Подтверждение пароля, лейбл и поле ввода
	m_Label_pass_confirm.set_text("Confirm your new password here:");
	m_Label_pass_confirm.set_margin(5);
	m_VBox.append(m_Label_pass_confirm);

	m_Entry_pass_confirm.set_max_length(99);
	m_Entry_pass_confirm.set_placeholder_text("Your new password again...");
	m_Entry_pass_confirm.set_margin(5);
	m_Entry_pass_confirm.set_visibility(false);
	m_Entry_pass_confirm.set_invisible_char('*');
	m_VBox.append(m_Entry_pass_confirm);

	// Кнопочки
	m_button_confirm.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &ChangePassWindow::on_confirm_click), "confirm"));
	m_button_cancel.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &ChangePassWindow::on_cancel_click)));

	m_check_button_alias_only.signal_toggled().connect(sigc::bind(
		sigc::mem_fun(*this, &ChangePassWindow::on_check_toggle)));

	m_HBox.append(m_button_cancel);
	m_button_cancel.set_margin(5);

	m_HBox.append(m_button_confirm);
	m_button_confirm.set_margin(5);

	m_HBox.set_halign(Gtk::Align::END);
	m_VBox.append(m_HBox);
	m_VBox.append(m_Label_error);
	m_Label_error.hide();
}

ChangePassWindow::~ChangePassWindow() {}

void ChangePassWindow::on_confirm_click(const Glib::ustring &data)
{
	Glib::ustring alias = m_Entry_alias.get_text();
	Glib::ustring password = m_Entry_pass.get_text();
	if (std::string(alias) != ALIAS && id_by_name(FILENAME, alias) != -1)
	{
		m_Label_error.set_markup("<span color='red'>That alias is already in use</span>");
		m_Label_error.show();
		return;
	}
	if (std::string(alias) == ALIAS)
		alias = "";

	if (password == m_Entry_pass_confirm.get_text() || m_check_button_alias_only.get_active())
	{
		change_password(FILENAME, ALIAS, std::string(alias), (m_check_button_alias_only.get_active() ? "" : std::string(password)));
		hide();
	}
	{
		m_Label_error.set_markup("<span color='red'>Passwords are different or fields are empty</span>");
		m_Label_error.show();
	}
}

void ChangePassWindow::on_cancel_click()
{
	hide();
	std::cout << "ChangePass - Cancel was pressed" << std::endl;
}

void ChangePassWindow::on_check_toggle()
{
	m_Entry_pass.set_can_focus(!m_check_button_alias_only.get_active());
	m_Entry_pass_confirm.set_can_focus(!m_check_button_alias_only.get_active());
}