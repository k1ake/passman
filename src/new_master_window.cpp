#include "new_master_window.h"
#include "main_window.h"

#include "master.h"
#include <iostream>
#include <gtkmm.h>

NewMasterWindow::NewMasterWindow(std::string filename)
	: m_button_cancel("Cancel"),
	  m_button_create("Create"),
	  m_VBox(Gtk::Orientation::VERTICAL),
	  m_Button_HBox(Gtk::Orientation::HORIZONTAL)
{
	FILENAME = filename;
	set_title("PassMan");
	set_default_size(350, 200);
	set_resizable(false);
	set_child(m_VBox);
	set_default_widget(m_button_create);

	m_VBox.set_margin(10);
	m_VBox.set_vexpand(true);

	m_Label_welcome.set_markup("Welcome to PassMan!");
	m_VBox.append(m_Label_welcome);

	// Создание мастер пароля
	m_Label_mpass.set_markup("Create your master password");
	m_Label_mpass.set_margin(5);
	m_VBox.append(m_Label_mpass);

	m_Entry_mpass.set_max_length(99);
	m_Entry_mpass.set_placeholder_text("Your password here...");
	m_Entry_mpass.set_margin(5);
	m_Entry_mpass.set_visibility(false);
	m_Entry_mpass.set_invisible_char('*');
	m_VBox.append(m_Entry_mpass);

	// Подтверждение мастер пароля
	m_Label_mpass_confirm.set_markup("Confirm your master password");
	m_Label_mpass_confirm.set_margin(5);
	m_VBox.append(m_Label_mpass_confirm);

	m_Entry_mpass_confirm.set_max_length(99);
	m_Entry_mpass_confirm.set_placeholder_text("Your password again...");
	m_Entry_mpass_confirm.set_margin(5);
	m_Entry_mpass_confirm.set_visibility(false);
	m_Entry_mpass_confirm.set_invisible_char('*');
	m_VBox.append(m_Entry_mpass_confirm);

	m_Button_HBox.append(m_button_cancel);
	m_button_cancel.set_margin(5);
	m_Button_HBox.append(m_button_create);
	m_button_create.set_margin(5);
	m_Button_HBox.set_halign(Gtk::Align::END);
	m_VBox.append(m_Button_HBox);

	m_VBox.append(m_Label_error);
	m_Label_error.hide();

	// Кнопочки
	m_button_cancel.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &NewMasterWindow::on_cancel_click)));
	m_button_create.signal_clicked().connect(sigc::bind(
		sigc::mem_fun(*this, &NewMasterWindow::on_create_click)));

	// Управление клавишами
	controller->signal_key_pressed().connect(
		sigc::mem_fun(*this, &NewMasterWindow::on_window_key_pressed), false);
	add_controller(controller);
};
NewMasterWindow::~NewMasterWindow() {}

void NewMasterWindow::on_create_click()
{
	Glib::ustring mpassword = m_Entry_mpass.get_text();
	auto app = Gtk::Application::create("org.k1ake.passman");
	if (mpassword != m_Entry_mpass_confirm.get_text() || mpassword == "")
	{
		m_Label_error.set_markup("<span color='red'>Passwords are different or fields are empty</span>");
		m_Label_error.show();
		return;
	}
	create_master(FILENAME, mpassword);
	hide();
	app->make_window_and_run<MainWindow>(0, nullptr, FILENAME);
}

void NewMasterWindow::on_cancel_click()
{
	hide();
}

bool NewMasterWindow::on_window_key_pressed(
	guint keyval, guint, Gdk::ModifierType state)
{
	// select the first radio button, when we press alt + 1
	if (keyval == GDK_KEY_Escape)
	{
		// close the window, when the 'esc' key is pressed
		hide();
		return true;
	}

	return false;
}